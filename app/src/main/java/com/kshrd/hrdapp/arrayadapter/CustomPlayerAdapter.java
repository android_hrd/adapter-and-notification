package com.kshrd.hrdapp.arrayadapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kshrd.hrdapp.R;

public class CustomPlayerAdapter extends BaseAdapter {

    private int[] mProfiles;
    private CharSequence[] mPlayers;
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    public CustomPlayerAdapter(Context context, int[] icons, CharSequence[] subjects) {
        this.mContext = context;
        this.mProfiles = icons;
        this.mPlayers = subjects;
        this.mLayoutInflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return mPlayers.length;
    }

    @Override
    public Object getItem(int position) {
        return mPlayers[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mLayoutInflater.inflate(R.layout.player_listview_custom, null);
        ImageView imageView = convertView.findViewById(R.id.image_icon);
        TextView textView = convertView.findViewById(R.id.textSubject);
        imageView.setImageResource(mProfiles[position]);
        textView.setText(mPlayers[position]);

        //convertView.setOnClickListener(v -> Log.d("TAG", textView.getText().toString()));

        return convertView;
    }
}
