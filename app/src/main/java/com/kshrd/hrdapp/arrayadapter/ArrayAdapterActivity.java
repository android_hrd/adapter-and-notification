package com.kshrd.hrdapp.arrayadapter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.kshrd.hrdapp.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ArrayAdapterActivity extends AppCompatActivity {

    private ListView listView;
    private List<String> players = new ArrayList<>();
    private CharSequence[] playerList = {"Messi", "Ronaldo"};
    private int[] profileList = {R.drawable.messi, R.drawable.messi};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_array_adapter);

//        for (int i=0; i<50; i++) {
//            players.add("Player-0" + (i + 1));
//        }

        listView = findViewById(R.id.list_view_item);

        CustomPlayerAdapter customPlayerAdapter = new CustomPlayerAdapter(this, profileList, playerList);
        listView.setAdapter(customPlayerAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object data = parent.getItemAtPosition(position);
                Log.d("TAG", data.toString());
            }
        });

//        ArrayAdapter<String> playerAdapter = new ArrayAdapter<>(this,
//                android.R.layout.simple_list_item_1, players);
//
//        listView.setAdapter(playerAdapter);

    }
}
