package com.kshrd.hrdapp.utils;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class FragmentHelper {

    static public void addFragment(AppCompatActivity activity, @IdRes int container, Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
            transaction.add(container, fragment);
            transaction.commit();
        }
    }

}
