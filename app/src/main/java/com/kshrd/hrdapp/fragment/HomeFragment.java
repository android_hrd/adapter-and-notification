package com.kshrd.hrdapp.fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.kshrd.hrdapp.DetailActivity;
import com.kshrd.hrdapp.MainActivity;
import com.kshrd.hrdapp.R;

public class HomeFragment extends Fragment {

    private Button btnGoDetail;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment_layout, container, false);

        btnGoDetail = view.findViewById(R.id.button_go_detail);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnGoDetail.setOnClickListener(v -> {
            if (Configuration.ORIENTATION_PORTRAIT == getResources().getConfiguration().orientation) {
                if (getActivity() instanceof DetailActivity) {
                    DetailActivity detailActivity = (DetailActivity) getActivity();
                    Fragment fragment = new DetailFragment();
                    detailActivity.replaceFragment(R.id.container, fragment);
                }
            }
        });

    }
}
