package com.kshrd.hrdapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.kshrd.hrdapp.R;
import com.kshrd.hrdapp.callback.ItemClickCallback;

import java.util.ArrayList;
import java.util.List;

public class DetailFragment extends Fragment implements ItemClickCallback {

    private ListView mListView;
    private ArrayAdapter<String> mAdapter;
    private List<String> mDetail = new ArrayList<>();

    ItemClickCallback callback;
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            callback = (ItemClickCallback) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment_layout, container, false);

        this.initUI(view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        for (int i = 0; i < 50; i++) {
            this.mDetail.add("user0" + (i + 1) + "@gmail.com");
        }
        mAdapter.notifyDataSetChanged();

        mListView.setOnItemClickListener((parent, view1, position, id) -> {
            callback.onCallbackWithObject(mDetail.get(position));
        });
    }

    private void initUI(View view) {
        mListView = view.findViewById(R.id.listView);
        mAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, mDetail);
        mListView.setAdapter(mAdapter);
    }

    public void setDetail(List<String> detail) {
        this.mDetail.addAll(detail);
    }

    @Override
    public void onCallbackWithObject(String s) {

    }
}
