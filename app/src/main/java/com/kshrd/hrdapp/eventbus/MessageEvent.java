package com.kshrd.hrdapp.eventbus;

public class MessageEvent {

    public MessageEvent(String message) {
        this.message = message;
    }

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
