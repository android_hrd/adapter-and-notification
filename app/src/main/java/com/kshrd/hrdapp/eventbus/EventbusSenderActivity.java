package com.kshrd.hrdapp.eventbus;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.kshrd.hrdapp.R;

import org.greenrobot.eventbus.EventBus;

public class EventbusSenderActivity extends AppCompatActivity {

    private EditText editMsg;

    private void initializeWidget() {
        this.editMsg = findViewById(R.id.editMsg);
    }

    private void postEventToSubscriber() {
        findViewById(R.id.button_sender).setOnClickListener(v->{
//            Toast.makeText(EventbusSenderActivity.this, "POSTED IN : " + Thread.currentThread(), Toast.LENGTH_LONG).show();
            String msg = editMsg.getText().toString();
            if (TextUtils.isEmpty(msg)) {
                msg = "Hello this is the default message";
            }
            MessageEvent event = new MessageEvent(msg);
            EventBus.getDefault().post(event);
            Toast.makeText(EventbusSenderActivity.this, event.getMessage(), Toast.LENGTH_LONG).show();
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.eventbus_sender_layout);
        this.initializeWidget();
        this.postEventToSubscriber();
    }
}
