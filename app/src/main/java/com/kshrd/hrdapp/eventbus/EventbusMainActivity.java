package com.kshrd.hrdapp.eventbus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.kshrd.hrdapp.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class EventbusMainActivity extends AppCompatActivity {

    private TextView mTextMessage;

    private void initializeWidget() {
        this.mTextMessage = findViewById(R.id.textMsg);
    }

    private void openSenderActivity() {
        findViewById(R.id.button_open_second_activity).setOnClickListener(v->{
            Intent intent = new Intent(EventbusMainActivity.this, EventbusSenderActivity.class);
            startActivity(intent);
        });
    }

    // 3. Execute in the thread that generated the event
//    @Subscribe(threadMode = ThreadMode.POSTING)
//    public void onMessageEventPostThread(MessageEvent msg) {
//        Log.e("Post thread", Thread.currentThread().getName());
//        mTextMessage.setText(msg.getMessage());
//    }

    // 4. Execute in the UI thread
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEventMainThread(MessageEvent msg) {
        Log.e("Main thread", Thread.currentThread().getName());
        mTextMessage.setText(msg.getMessage());
        Toast.makeText(EventbusMainActivity.this, msg.getMessage(), Toast.LENGTH_LONG).show();
    }

    // 5. Execute in the background thread
//    @Subscribe(sticky = true, threadMode = ThreadMode.BACKGROUND)
//    public void onMessageEventBackgroundThread(MessageEvent msg) {
//        Log.e("Background thread", Thread.currentThread().getName());
//    }

    // 6. Whether it is ui thread or not, it is executed in a new thread
//    @Subscribe(sticky = true, threadMode = ThreadMode.ASYNC)
//    public void onMessageEventAsync(MessageEvent msg) {
//        Log.e("Async thread", Thread.currentThread().getName());
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.eventbus_main_layout);
        initializeWidget();
        openSenderActivity();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
