package com.kshrd.hrdapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.kshrd.hrdapp.fragment.DetailFragment;
import com.kshrd.hrdapp.fragment.HomeFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAdd;
    private Button btnReplace;
    private Button btnRemove;

    static final String HOME_FRAGMENT_TAG = "Home Fragment";
    static final String DETAIL_FRAGMENT_TAG = "Detail Fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAdd = findViewById(R.id.button_add);
        btnReplace = findViewById(R.id.button_replace);
        btnRemove = findViewById(R.id.button_remove);

        btnAdd.setOnClickListener(this);
        btnReplace.setOnClickListener(this);
        btnRemove.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {
            case R.id.button_add:

                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                // Create fragment object
                HomeFragment homeFragment = new HomeFragment();
                fragmentTransaction.add(R.id.container, homeFragment, HOME_FRAGMENT_TAG);
                fragmentTransaction.commit();

                break;
            case R.id.button_replace:

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                // Create fragment object
                DetailFragment detailFragment = new DetailFragment();
                transaction.replace(R.id.container, detailFragment, DETAIL_FRAGMENT_TAG);
                transaction.addToBackStack(null);
                transaction.commit();

                break;
            case R.id.button_remove:
                Toast.makeText(this, "Remove", Toast.LENGTH_LONG).show();
                break;
        }

    }
}
