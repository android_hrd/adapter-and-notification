package com.kshrd.hrdapp.customdialog;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.kshrd.hrdapp.R;

public class DialogActivity extends AppCompatActivity implements CustomDialog.CustomDialogListener {

    final static String CUSTOM_DIALOG_TAG = "Login Dialog Customization";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        final Button btnShowDialog = findViewById(R.id.button_dialog);

        btnShowDialog.setOnClickListener(v -> {

            CustomDialog customDialog = new CustomDialog();
            customDialog.setCancelable(false);
            customDialog.show(getSupportFragmentManager(), CUSTOM_DIALOG_TAG);

        });

    }

    @Override
    public void getInput(String email, String password, boolean isRememberMe) {
        Toast.makeText(this,
                email + "-" + password + "-" + isRememberMe,
                Toast.LENGTH_LONG).show();
    }
}
