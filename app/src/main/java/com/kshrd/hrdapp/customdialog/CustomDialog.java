package com.kshrd.hrdapp.customdialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.kshrd.hrdapp.R;

public class CustomDialog extends DialogFragment {

    CustomDialogListener customDialogListener;

    public interface CustomDialogListener {
        void getInput(String email, String password, boolean isRememberMe);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // set header
//        builder.setTitle("Custom Dialog");

        // set body
        final View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.custom_dialog, null);
        builder.setView(view);

        final EditText mEmail = view.findViewById(R.id.edit_email);
        final EditText mPassword = view.findViewById(R.id.edit_password);
        final CheckBox mRememberMe = view.findViewById(R.id.check_remember);
        final Button mCancel = view.findViewById(R.id.button1);
        final Button mLogin = view.findViewById(R.id.button2);

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();
                boolean isRememberMe = mRememberMe.isChecked();
                customDialogListener.getInput(email, password, isRememberMe);
                dismiss();
            }
        });

        // set footer
        /*builder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();
                boolean isRememberMe = mRememberMe.isChecked();
                customDialogListener.getInput(email, password, isRememberMe);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });*/

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            customDialogListener = (CustomDialogListener) context;
        } catch (ClassCastException ex) {
            throw new ClassCastException(context.toString() + "Must implement");
        }
    }
}
