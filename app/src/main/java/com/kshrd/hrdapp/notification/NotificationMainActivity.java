package com.kshrd.hrdapp.notification;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;

import com.kshrd.hrdapp.R;

public class NotificationMainActivity extends AppCompatActivity {

    final static String CHANNEL_ID = "100";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_main);

        final Button btnPushNotification = findViewById(R.id.button_notification);

        btnPushNotification.setOnClickListener(v->{

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .setContentTitle("Chan Chhaya")
                    .setContentText("Hey girl..!")
                    .build();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "REKSMEY", NotificationManager.IMPORTANCE_HIGH);
                channel.setDescription("Demo Project");

                NotificationManager manager = getSystemService(NotificationManager.class);
                manager.createNotificationChannel(channel);
            }

            NotificationManagerCompat managerCompat = NotificationManagerCompat.from(this);

            managerCompat.notify(1, notification);

        });

    }
}
