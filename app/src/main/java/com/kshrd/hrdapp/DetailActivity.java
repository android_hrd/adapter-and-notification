package com.kshrd.hrdapp;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;

import com.kshrd.hrdapp.callback.ItemClickCallback;
import com.kshrd.hrdapp.fragment.DetailFragment;
import com.kshrd.hrdapp.fragment.HomeFragment;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity implements ItemClickCallback {

    static final String FRAGMENT_TAG = "Fragment Add";

    private Configuration mNewConfig = new Configuration();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Fragment homeFragment = new HomeFragment();
        Fragment detailFragment = new DetailFragment();

        if (mNewConfig.ORIENTATION_PORTRAIT == getResources().getConfiguration().orientation) {
            this.addFragment(R.id.container, homeFragment);
        } else if (mNewConfig.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
            this.addFragment(R.id.left_container, homeFragment);
            this.addFragment(R.id.right_container, detailFragment);
        }

    }

    public void addFragment(@IdRes int container, Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (fragment != null) {
            transaction.add(container, fragment);
            transaction.commit();
        }
    }

    public void replaceFragment(@IdRes int container, Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (fragment != null) {
            transaction.replace(container, fragment);
            transaction.addToBackStack(FRAGMENT_TAG);
            transaction.commit();
        }
    }

    @Override
    public void onCallbackWithObject(String s) {
        Toast.makeText(this, "Email : " + s, Toast.LENGTH_SHORT).show();
    }
}
