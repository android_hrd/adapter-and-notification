package com.kshrd.hrdapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import com.kshrd.hrdapp.fragment.DetailFragment;
import com.kshrd.hrdapp.utils.FragmentHelper;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    private List<String> mDetail = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        for (int i = 0; i < 50; i++) {
            this.mDetail.add("Admin " + (i + 1));
        }

        Fragment detailFragment = new DetailFragment();
        ((DetailFragment) detailFragment).setDetail(mDetail);
        FragmentHelper.addFragment(this, R.id.list_container, detailFragment);
    }
}
